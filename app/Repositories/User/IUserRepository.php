<?php

namespace App\Repositories\User;

interface IUserRepository {
    public function getUserByEmail($email);
    public function createUser($collection = []);
}
