<?php

namespace Tests\Unit;

use Tests\TestCase;

class CarTest extends TestCase
{
    public function test_crud()
    {
        $data = [
            'email' => 'haris@gmail.com',
            'password' => '123456'
        ];
        $response = $this->post(route('api.auth.login'), $data)->assertStatus(200)->assertJsonStructure(['data' => ['user', 'token']])->decodeResponseJson();
        $token = 'Bearer ' . $response['data']['token'];

        $data = [
            'year' => '2022',
            'color' => 'red',
            'price' => 20000000
        ];

        $response = $this->withHeader('Authorization', $token)->post(route('api.vehicles.store'), $data)->assertStatus(200)->decodeResponseJson();
        $vehicleId = $response['data']['_id'];

        $data = [
            'vehicle_id' => $vehicleId,
            'engine' => 'skyactive',
            'capacity' => 8,
            'type' => 'manual'
        ];

        $response = $this->withHeader('Authorization', $token)->post(route('api.cars.store'), $data)->assertStatus(200)->decodeResponseJson();
        $id = $response['data']['_id'];

        $this->withHeader('Authorization', $token)->get(route('api.cars.index'))->assertStatus(200);
        $this->withHeader('Authorization', $token)->get(route('api.cars.show', $id))->assertStatus(200);
        $this->withHeader('Authorization', $token)->put(route('api.cars.update', $id), $data)->assertStatus(200);
        $this->withHeader('Authorization', $token)->delete(route('api.cars.destroy', $id))->assertStatus(200);

        $this->withHeader('Authorization', $token)->delete(route('api.vehicles.destroy', $vehicleId))->assertStatus(200);

        $this->withHeader('Authorization', $token)->get(route('api.auth.logout'))->assertStatus(200);
    }
}
