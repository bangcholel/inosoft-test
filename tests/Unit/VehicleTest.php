<?php

namespace Tests\Unit;

use Tests\TestCase;

class VehicleTest extends TestCase
{
    public function test_crud()
    {
        $data = [
            'email' => 'haris@gmail.com',
            'password' => '123456'
        ];
        $response = $this->post(route('api.auth.login'), $data)->assertStatus(200)->assertJsonStructure(['data' => ['user', 'token']])->decodeResponseJson();
        $token = 'Bearer ' . $response['data']['token'];

        $data = [
            'year' => '2022',
            'color' => 'red',
            'price' => 20000000
        ];

        $response = $this->withHeader('Authorization', $token)->post(route('api.vehicles.store'), $data)->assertStatus(200)->decodeResponseJson();
        $id = $response['data']['_id'];

        $this->withHeader('Authorization', $token)->get(route('api.vehicles.index'))->assertStatus(200);
        $this->withHeader('Authorization', $token)->get(route('api.vehicles.show', $id))->assertStatus(200);
        $this->withHeader('Authorization', $token)->put(route('api.vehicles.update', $id), $data)->assertStatus(200);
        $this->withHeader('Authorization', $token)->delete(route('api.vehicles.destroy', $id))->assertStatus(200);

        $this->withHeader('Authorization', $token)->get(route('api.auth.logout'))->assertStatus(200);
    }
}
