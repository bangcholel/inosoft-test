<?php
declare(strict_types=1);

namespace App\Repositories\Sale;

use App\Models\Car;
use App\Models\Motorcycle;
use App\Models\Sale;
use Carbon\Carbon;
use MongoDB\BSON\ObjectID;

class SaleRepository implements ISaleRepository {

    public function getSales()
    {
        $sales = Sale::orderBy('created_at', 'DESC');
        if (request()->has('created_at')) {
            foreach(request()->get('created_at') as $key => $value) {
                $splitDate = explode('-', $value);
                if ($key == 'gte' || $key == 'gt') {
                    $sales->where('created_at', '>=', Carbon::createFromDate(intval($splitDate[0]),intval($splitDate[1]),intval($splitDate[2])));
                }
                if ($key == 'lte' || $key == 'lt') {
                    $sales->where('created_at', '<=', Carbon::createFromDate(intval($splitDate[0]),intval($splitDate[1]),intval($splitDate[2])));
                }
            }
        }
        if (request()->has('motorcycle_id')) {
            $q = request()->get('motorcycle_id');
            $sales->where('motorcycles.motorcycle_id', '=', $q);
        }
        if (request()->has('car_id')) {
            $q = request()->get('car_id');
            $sales->where('cars.car_id', '=', $q);
        }
        if (request()->has('vehicle_id')) {
            $q = request()->get('vehicle_id');
            $sales->where('cars.vehicle_id', '=', $q);
        }
        $sales = $sales->get();
        return $sales;
    }

    public function createSale($collection = [])
    {
        $totalPrice = 0;
        $motorcycleTotalPrice = 0;
        $carTotalPrice = 0;
        $totalQuantity = 0;
        $motorcycleQuantity = 0;
        $carQuantity = 0;
        $totalItem = 0;
        $data = [];

        $dataCars = [];
        if (isset($collection['car_id']) && isset($collection['car_qty'])) {
            $carIds = [];
            foreach($collection['car_id'] as $id) {
                array_push($carIds, new ObjectID($id));
            }
            $cars = Car::whereIn('_id', $carIds)->get();
            $isStockEnough = true;
            for ($i = 0; $i < count($cars); $i++) {
                $stock = $cars[$i]->stock;
                $stock -= $collection['car_qty'][$i];
                if ($stock < 0) {
                    $isStockEnough = false;
                }
                $totalPrice += $collection['car_qty'][$i] * $cars[$i]->vehicle['price'];
                $totalQuantity += $collection['car_qty'][$i];
                $carTotalPrice += $collection['car_qty'][$i] * $cars[$i]->vehicle['price'];
                $carQuantity += $collection['car_qty'][$i];
                $totalItem++;
                array_push($dataCars, [
                    'car_id' => $cars[$i]->_id,
                    'vehicle_id' => (string)$cars[$i]->vehicle_id,
                    'engine' => $cars[$i]->engine,
                    'capacity' => $cars[$i]->capacity,
                    'type' => $cars[$i]->type,
                    'vehicle' => [
                        'year' => $cars[$i]->vehicle['year'],
                        'color' => $cars[$i]->vehicle['color'],
                        'price' => $cars[$i]->vehicle['price'],
                    ],
                    'quantity' => $collection['car_qty'][$i],
                    'price' => $collection['car_qty'][$i] * $cars[$i]->vehicle['price']
                ]);
            }
            if (!$isStockEnough) {
                throw new \Exception('Car stock is not enough');
            }
            for ($i = 0; $i < count($cars); $i++) {
                $stock = $cars[$i]->stock;
                $stock -= $collection['car_qty'][$i];
                $cars[$i]->stock = $stock;
                $cars[$i]->save();
            }
        }

        $dataMotorcycles = [];
        if (isset($collection['motorcycle_id']) && isset($collection['motorcycle_qty'])) {
            $motorcycleIds = [];
            foreach($collection['motorcycle_id'] as $id) {
                array_push($motorcycleIds, new ObjectID($id));
            }
            $motorcycles = Motorcycle::whereIn('_id', $motorcycleIds)->get();
            $isStockEnough = true;
            for ($i = 0; $i < count($motorcycles); $i++) {
                $stock = $motorcycles[$i]->stock;
                $stock -= $collection['motorcycle_qty'][$i];
                if ($stock < 0) {
                    $isStockEnough = false;
                }
                $totalPrice += $collection['motorcycle_qty'][$i] * $motorcycles[$i]->vehicle['price'];
                $totalQuantity += $collection['motorcycle_qty'][$i];
                $motorcycleTotalPrice += $collection['motorcycle_qty'][$i] * $motorcycles[$i]->vehicle['price'];
                $motorcycleQuantity += $collection['motorcycle_qty'][$i];
                $totalItem++;
                array_push($dataMotorcycles, [
                    'motorcycle_id' => $motorcycles[$i]->_id,
                    'vehicle_id' => (string)$motorcycles[$i]->vehicle_id,
                    'engine' => $motorcycles[$i]->engine,
                    'suspension' => $motorcycles[$i]->suspension,
                    'transmission' => $motorcycles[$i]->transmission,
                    'vehicle' => [
                        'year' => $motorcycles[$i]->vehicle['year'],
                        'color' => $motorcycles[$i]->vehicle['color'],
                        'price' => $motorcycles[$i]->vehicle['price'],
                    ],
                    'quantity' => $collection['motorcycle_qty'][$i],
                    'price' => $collection['motorcycle_qty'][$i] * $motorcycles[$i]->vehicle['price'],
                ]);
            }
            if (!$isStockEnough) {
                throw new \Exception('Motorcycle stock is not enough');
            }
            for ($i = 0; $i < count($motorcycles); $i++) {
                $stock = $motorcycles[$i]->stock;
                $stock -= $collection['motorcycle_qty'][$i];
                $motorcycles[$i]->stock = $stock;
                $motorcycles[$i]->save();
            }
        }

        $data = [
            'total_price' => $totalPrice,
            'motorcycle_total_price' => $motorcycleTotalPrice,
            'car_total_price' => $carTotalPrice,
            'total_item' => $totalItem,
            'total_quantity' => $totalQuantity,
            'motorcycle_quantity' => $motorcycleQuantity,
            'car_quantity' => $carQuantity,
            'motorcycles' => $dataMotorcycles,
            'cars' => $dataCars,
        ];
        $sale = Sale::create($data);
        return $sale;
    }

    public function getSale($id)
    {
        return Sale::find($id);
    }
}
