<?php
declare(strict_types=1);

namespace App\Services;

use App\Repositories\Vehicle\VehicleRepository;

class VehicleService {

    protected $vehicleRepository;

    public function __construct(VehicleRepository $vehicleRepository) {
        $this->vehicleRepository = $vehicleRepository;
    }

    public function getVehicles() {
        return $this->vehicleRepository->getVehicles();
    }

    public function createVehicle($collection = []) {
        return $this->vehicleRepository->createVehicle($collection);
    }

    public function getVehicle($id) {
        return $this->vehicleRepository->getVehicle($id);
    }

    public function updateVehicle($id, $collection = []) {
        return $this->vehicleRepository->updateVehicle($id, $collection);
    }

    public function deleteVehicle($id) {
        return $this->vehicleRepository->deleteVehicle($id);
    }
}
