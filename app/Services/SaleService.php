<?php
declare(strict_types=1);

namespace App\Services;

use App\Repositories\Sale\SaleRepository;

class SaleService {

    protected $saleRepository;

    public function __construct(SaleRepository $saleRepository) {
        $this->saleRepository = $saleRepository;
    }

    public function getSales() {
        return $this->saleRepository->getSales();
    }

    public function createSale($collection = []) {
        if (!isset($collection['car_id']) && !isset($collection['car_qty']) &&
            !isset($collection['motorcycle_id']) && !isset($collection['motorcycle_qty'])) {
            throw new \Exception('Please choose one vehicle');
        }
        if (isset($collection['car_id']) && !isset($collection['car_qty'])) {
            throw new \Exception('Please input car qty');
        }
        if (!isset($collection['car_id']) && isset($collection['car_qty'])) {
            throw new \Exception('Please input car id');
        }
        if (isset($collection['motorcycle_id']) && !isset($collection['motorcycle_qty'])) {
            throw new \Exception('Please input motorcycle qty');
        }
        if (!isset($collection['motorcycle_id']) && isset($collection['motorcycle_qty'])) {
            throw new \Exception('Please input motorcycle id');
        }
        if (isset($collection['car_id']) && isset($collection['car_qty'])) {
            $newCarId = [];
            $newCarQty = [];
            for($i = 0; $i < count($collection['car_id']); $i++) {
                if (in_array($collection['car_id'][$i], $newCarId)) {
                    $index = array_search($collection['car_id'][$i], $newCarId);
                    $total = $newCarQty[$index] + intval($collection['car_qty'][$i]);
                    $newCarQty[$index] = $total;
                } else {
                    array_push($newCarId, $collection['car_id'][$i]);
                    array_push($newCarQty, intval($collection['car_qty'][$i]));
                }
            }
            $collection['car_id'] = $newCarId;
            $collection['car_qty'] = $newCarQty;
        }
        if (isset($collection['motorcycle_id']) && isset($collection['motorcycle_qty'])) {
            $newMotorcycleId = [];
            $newMotorcycleQty = [];
            for($i = 0; $i < count($collection['motorcycle_id']); $i++) {
                if (in_array($collection['motorcycle_id'][$i], $newMotorcycleId)) {
                    $index = array_search($collection['motorcycle_id'][$i], $newMotorcycleId);
                    $total = $newMotorcycleQty[$index] + intval($collection['motorcycle_qty'][$i]);
                    $newMotorcycleQty[$index] = $total;
                } else {
                    array_push($newMotorcycleId, $collection['motorcycle_id'][$i]);
                    array_push($newMotorcycleQty, intval($collection['motorcycle_qty'][$i]));
                }
            }
            $collection['motorcycle_id'] = $newMotorcycleId;
            $collection['motorcycle_qty'] = $newMotorcycleQty;
        }
        return $this->saleRepository->createSale($collection);
    }

    public function getSale($id) {
        return $this->saleRepository->getSale($id);
    }
}
