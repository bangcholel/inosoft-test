<?php
declare(strict_types=1);

namespace App\Repositories\Vehicle;

use App\Models\Car;
use App\Models\Motorcycle;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MongoDB\BSON\ObjectID;

class VehicleRepository implements IVehicleRepository {

    public function getVehicles()
    {
        $motorcycles = Vehicle::orderBy('created_at', 'DESC');
        if (request()->has('price')) {
            $arr = [];
            foreach(request()->get('price') as $key => $value) {
                $arr = array_merge($arr, ['$' . $key => intval($value)]);
            }
            $motorcycles = $motorcycles->whereRaw(['vehicle.price' => $arr]);
        }
        if (request()->has('color')) {
            $q = request()->get('color');
            $motorcycles = $motorcycles->where('color', '=', $q);
        }
        if (request()->has('year')) {
            $q = request()->get('year');
            $motorcycles = $motorcycles->where('year', '=', $q);
        }
        $motorcycles = $motorcycles->get();
        return $motorcycles;
    }

    public function createVehicle($collection = [])
    {
        $vehicle = Vehicle::create([
            'year' => $collection['year'],
            'color' => $collection['color'],
            'price' => doubleval($collection['price']),
        ]);

        return $vehicle;
    }

    public function getVehicle($id)
    {
        return Vehicle::find($id);
    }

    public function updateVehicle($id, $collection = [])
    {
        $vehicle = Vehicle::find($id);
        if ($vehicle == null) {
            throw new ModelNotFoundException();
        }
        $vehicle->year = $collection['year'];
        $vehicle->color = $collection['color'];
        $vehicle->price = doubleval($collection['price']);
        $vehicle->save();

        Car::where('vehicle_id', '=', new ObjectId($id))->update(['vehicle' => [
            'year' => $vehicle->year,
            'color' => $vehicle->color,
            'price' => $vehicle->price,
        ]], ['upsert' => false]);

        Motorcycle::where('vehicle_id', '=', new ObjectId($id))->update(['vehicle' => [
            'year' => $vehicle->year,
            'color' => $vehicle->color,
            'price' => $vehicle->price,
        ]], ['upsert' => false]);

        return Vehicle::find($id);
    }

    public function deleteVehicle($id)
    {
        $vehicle = Vehicle::find($id);
        if ($vehicle == null) {
            throw new ModelNotFoundException();
        }
        $vehicle->delete();

        return $vehicle;
    }
}
