<?php

namespace App\Repositories\Car;

interface ICarRepository {
    public function getCars();
    public function createCar($collection = []);
    public function getCar($id);
    public function updateCar($id, $collection = []);
    public function deleteCar($id);
}
