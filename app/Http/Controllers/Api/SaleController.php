<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Services\SaleService;
use Illuminate\Http\Request;

class SaleController extends ApiController
{
    protected $saleService;

    public function __construct(SaleService $saleService)
    {
        $this->saleService = $saleService;
    }

    public function index() {
        return $this->showData($this->saleService->getSales());
    }

    public function store(Request $request) {
        $request->validate([
            'motorcycle_id.*' => ['sometimes', 'exists:motorcycles,_id'],
            'motorcycle_qty.*' => ['sometimes', 'numeric'],
            'car_id.*' => ['sometimes', 'exists:cars,_id'],
            'car_qty.*' => ['sometimes', 'numeric'],
        ]);

        return $this->showData($this->saleService->createSale($request->all()));
    }

    public function show($id) {
        return $this->showData($this->saleService->getSale($id));
    }
}
