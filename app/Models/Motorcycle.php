<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Motorcycle extends Model
{
    use HasFactory;

    protected $fillable = [
        'vehicle_id',
        'engine',
        'suspension',
        'transmission',
        'stock',
        'vehicle',
    ];
}
