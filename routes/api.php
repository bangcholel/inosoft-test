<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CarController;
use App\Http\Controllers\Api\MotorcycleController;
use App\Http\Controllers\Api\SaleController;
use App\Http\Controllers\Api\VehicleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['as' => 'api.'], function() {

    Route::group(['prefix' => 'auth', 'as' => 'auth.'], function() {
        Route::post('login', [AuthController::class, 'login'])->name('login');
        Route::post('register', [AuthController::class, 'register'])->name('register');

        Route::group(['middleware' => 'jwt.verify'], function() {
            Route::get('logout', [AuthController::class, 'logout'])->name('logout');
        });
    });

    Route::group(['middleware' => 'jwt.verify'], function() {
        Route::resource('vehicles', VehicleController::class);
        Route::resource('motorcycles', MotorcycleController::class);
        Route::resource('cars', CarController::class);
        Route::resource('sales', SaleController::class)->only(['index', 'store', 'show']);
    });
});
