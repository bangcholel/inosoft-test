<?php
declare(strict_types=1);

namespace App\Repositories\Motorcycle;

use App\Models\Motorcycle;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MongoDB\BSON\ObjectID;

class MotorcycleRepository implements IMotorcycleRepository {

    public function getMotorcycles()
    {
        $motorcycles = Motorcycle::orderBy('created_at', 'DESC');
        if (request()->has('stock')) {
            $arr = [];
            foreach(request()->get('stock') as $key => $value) {
                $arr = array_merge($arr, ['$' . $key => intval($value)]);
            }
            $motorcycles = $motorcycles->whereRaw(['stock' => $arr]);
        }
        if (request()->has('price')) {
            $arr = [];
            foreach(request()->get('price') as $key => $value) {
                $arr = array_merge($arr, ['$' . $key => intval($value)]);
            }
            $motorcycles = $motorcycles->whereRaw(['vehicle.price' => $arr]);
        }
        if (request()->has('transmission')) {
            $q = request()->get('transmission');
            $motorcycles = $motorcycles->where('transmission', '=', $q);
        }
        if (request()->has('suspension')) {
            $q = request()->get('suspension');
            $motorcycles = $motorcycles->where('suspension', '=', $q);
        }
        if (request()->has('engine')) {
            $q = request()->get('engine');
            $motorcycles = $motorcycles->where('engine', 'like', '%' . $q . '%');
        }
        if (request()->has('year')) {
            $q = request()->get('year');
            $motorcycles = $motorcycles->where('vehicle.year', '=', $q);
        }
        if (request()->has('color')) {
            $q = request()->get('color');
            $motorcycles = $motorcycles->where('vehicle.color', '=', $q);
        }
        $motorcycles = $motorcycles->get();
        return $motorcycles;
    }

    public function createMotorcycle($collection = [])
    {
        if (array_key_exists('stock', $collection)) {
            $stock = intval($collection['stock']);
        } else {
            $stock = 0;
        }
        $vehicle = Vehicle::find($collection['vehicle_id']);
        if ($vehicle == null) {
            throw new ModelNotFoundException();
        }
        $motorcycle = Motorcycle::create([
            'vehicle_id' => new ObjectId($collection['vehicle_id']),
            'stock' => $stock,
            'engine' => $collection['engine'],
            'suspension' => $collection['suspension'],
            'transmission' => $collection['transmission'],
            'vehicle' => [
                'year' => $vehicle->year,
                'color' => $vehicle->color,
                'price' => doubleval($vehicle->price),
            ]
        ]);

        return $motorcycle;
    }

    public function getMotorcycle($id)
    {
        return Motorcycle::where('_id', $id)->first();
    }

    public function updateMotorcycle($id, $collection = [])
    {
        $motorcycle = Motorcycle::find($id);
        if ($motorcycle == null) {
            throw new ModelNotFoundException();
        }
        $vehicle = Vehicle::find($collection['vehicle_id']);
        if ($vehicle == null) {
            throw new ModelNotFoundException();
        }
        $motorcycle->vehicle_id = $collection['vehicle_id'];
        $motorcycle->engine = $collection['engine'];
        if (array_key_exists('stock', $collection)) {
            $motorcycle->stock = intval($collection['stock']);
        }
        $motorcycle->suspension = $collection['suspension'];
        $motorcycle->transmission = $collection['transmission'];
        $motorcycle->vehicle = [
            'year' => $vehicle->year,
            'color' => $vehicle->color,
            'price' => doubleval($vehicle->price),
        ];
        $motorcycle->save();

        return Motorcycle::find($id);
    }

    public function deleteMotorcycle($id)
    {
        $motorcycle = Motorcycle::find($id);
        if ($motorcycle == null) {
            throw new ModelNotFoundException();
        }
        $motorcycle->delete();

        return $motorcycle;
    }
}
