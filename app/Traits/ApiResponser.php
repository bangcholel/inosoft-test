<?php

namespace App\Traits;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

trait ApiResponser
{
    private function convertToResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    protected function errorResponse($msg, $code, $data = null)
    {
        $d = [
            'error' => [
                'msg' => $msg,
                'data' => $data
            ],
            'code' => $code
        ];
        return $this->convertToResponse($d, $code);
    }

    protected function errorUnAuthenticatedResponse()
    {
        return $this->errorResponse('Who are you ?', 401);
    }

    protected function showAll(Collection $collection, $code = 200)
    {
        if ($collection->isEmpty()) {
            return $this->convertToResponse(['data' => $collection], $code);
        }

        $collection = $this->sortData($collection);
        if (request()->has('limit') && (int)request()->get('limit') <= 0) {
            if ((int)request()->get('limit') == 0) {
                $collection = ['data' => []];
            } else {
                $collection = ['data' => $collection];
            }
        } else {
            $collection = $this->paginate($collection);
        }

        return $this->convertToResponse($collection, $code);
    }

    protected function showData($data, $code = 200)
    {
        $d = [
            'data' => $data
        ];
        if ($data == null) {
            return $this->errorResponse(['msg' => 'Data not found !'], 400);
        }
        return $this->convertToResponse($d, $code);
    }

    protected function sortData(Collection $collection)
    {
        if (request()->has('sort_by')) {
            $attribute = request()->sort_by;
            $collection = $collection->sortBy->{$attribute};
        }
        return $collection;
    }

    protected function paginate(Collection $collection, $defaultPage = 20)
    {
        $rules = [
            'per_page' => 'integer|min:2|max:50'
        ];

        Validator::validate(request()->all(), $rules);

        $page = LengthAwarePaginator::resolveCurrentPage();

        $perPage = $defaultPage;

        if (request()->has('limit')) {
            $perPage = (int)request()->get('limit');
        }
        if (request()->has('per_page')) {
            $perPage = (int)request()->per_page;
        }

        $results = $collection->slice(($page - 1) * $perPage, $perPage)->values();

        $paginated = new LengthAwarePaginator($results, $collection->count(), $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);

        $paginated->appends(request()->all());

        return $paginated;
    }
}
