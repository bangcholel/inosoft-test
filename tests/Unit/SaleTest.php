<?php

namespace Tests\Unit;

use Tests\TestCase;

class SaleTest extends TestCase
{
    public function test_crud()
    {
        $data = [
            'email' => 'haris@gmail.com',
            'password' => '123456'
        ];
        $response = $this->post(route('api.auth.login'), $data)->assertStatus(200)->assertJsonStructure(['data' => ['user', 'token']])->decodeResponseJson();
        $token = 'Bearer ' . $response['data']['token'];

        $data = [
            'car_id' => ['6371084fa1648217cc4b2168'],
            'car_qty' => [3],
            'motorcycle_id' => ['637109a1a1648217cc4b216a'],
            'motorcycle_qty' => [4],
        ];

        $response = $this->withHeader('Authorization', $token)->post(route('api.sales.store'), $data)->assertStatus(200)->decodeResponseJson();
        $id = $response['data']['_id'];

        $this->withHeader('Authorization', $token)->get(route('api.sales.index'))->assertStatus(200);
        $this->withHeader('Authorization', $token)->get(route('api.sales.show', $id))->assertStatus(200);

        $this->withHeader('Authorization', $token)->get(route('api.auth.logout'))->assertStatus(200);
    }
}
