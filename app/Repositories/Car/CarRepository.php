<?php
declare(strict_types=1);

namespace App\Repositories\Car;

use App\Models\Car;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MongoDB\BSON\ObjectID;

class CarRepository implements ICarRepository {

    public function getCars()
    {
        $cars = Car::orderBy('created_at', 'DESC');
        if (request()->has('stock')) {
            $arr = [];
            foreach(request()->get('stock') as $key => $value) {
                $arr = array_merge($arr, ['$' . $key => intval($value)]);
            }
            $cars = $cars->whereRaw(['stock' => $arr]);
        }
        if (request()->has('capacity')) {
            $arr = [];
            foreach(request()->get('capacity') as $key => $value) {
                $arr = array_merge($arr, ['$' . $key => intval($value)]);
            }
            $cars = $cars->whereRaw(['capacity' => $arr]);
        }
        if (request()->has('price')) {
            $arr = [];
            foreach(request()->get('price') as $key => $value) {
                $arr = array_merge($arr, ['$' . $key => intval($value)]);
            }
            $cars = $cars->whereRaw(['vehicle.price' => $arr]);
        }
        if (request()->has('type')) {
            $q = request()->get('type');
            $cars = $cars->where('type', '=', $q);
        }
        if (request()->has('engine')) {
            $q = request()->get('engine');
            $cars = $cars->where('engine', 'like', '%' . $q . '%');
        }
        if (request()->has('year')) {
            $q = request()->get('year');
            $cars = $cars->where('vehicle.year', '=', $q);
        }
        if (request()->has('color')) {
            $q = request()->get('color');
            $cars = $cars->where('vehicle.color', '=', $q);
        }
        $cars = $cars->get();
        return $cars;
    }

    public function createCar($collection = [])
    {
        if (array_key_exists('stock', $collection)) {
            $stock = intval($collection['stock']);
        } else {
            $stock = 0;
        }
        $vehicle = Vehicle::find($collection['vehicle_id']);
        if ($vehicle == null) {
            throw new ModelNotFoundException();
        }
        $car = Car::create([
            'vehicle_id' => new ObjectId($collection['vehicle_id']),
            'stock' => $stock,
            'engine' => $collection['engine'],
            'capacity' => intval($collection['capacity']),
            'type' => $collection['type'],
            'vehicle' => [
                'year' => $vehicle->year,
                'color' => $vehicle->color,
                'price' => doubleval($vehicle->price),
            ]
        ]);

        return $car;
    }

    public function getCar($id)
    {
        return Car::where('_id', $id)->first();
    }

    public function updateCar($id, $collection = [])
    {
        $car = Car::find($id);
        if ($car == null) {
            throw new ModelNotFoundException();
        }
        $vehicle = Vehicle::find($collection['vehicle_id']);
        if ($vehicle == null) {
            throw new ModelNotFoundException();
        }
        $car->vehicle_id = $collection['vehicle_id'];
        $car->engine = $collection['engine'];
        if (array_key_exists('stock', $collection)) {
            $car->stock = intval($collection['stock']);
        }
        $car->capacity = intval($collection['capacity']);
        $car->type = $collection['type'];
        $car->vehicle = [
            'year' => $vehicle->year,
            'color' => $vehicle->color,
            'price' => doubleval($vehicle->price),
        ];
        $car->save();

        return Car::find($id);
    }

    public function deleteCar($id)
    {
        $car = Car::find($id);
        if ($car == null) {
            throw new ModelNotFoundException();
        }
        $car->delete();

        return $car;
    }
}
