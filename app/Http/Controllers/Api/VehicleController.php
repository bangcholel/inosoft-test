<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Services\VehicleService;
use Illuminate\Http\Request;

class VehicleController extends ApiController
{
    protected $vehicleService;

    public function __construct(VehicleService $vehicleService)
    {
        $this->vehicleService = $vehicleService;
    }

    public function index() {
        return $this->showAll($this->vehicleService->getVehicles());
    }

    public function store(Request $request) {
        $request->validate([
            'year' => ['required', 'string', 'min:4', 'max:4'],
            'color' => ['required', 'string', 'min:3', 'max:255'],
            'price' => ['required', 'numeric']
        ]);

        return $this->showData($this->vehicleService->createVehicle($request->all()));
    }

    public function show($id) {
        return $this->showData($this->vehicleService->getVehicle($id));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'year' => ['required', 'string', 'min:4', 'max:4'],
            'color' => ['required', 'string', 'min:1', 'max:255'],
            'price' => ['required', 'numeric']
        ]);

        return $this->showData($this->vehicleService->updateVehicle($id, $request->all()));
    }

    public function destroy($id) {
        return $this->showData($this->vehicleService->deleteVehicle($id));
    }
}
