<?php
declare(strict_types=1);

namespace App\Repositories\User;

use App\Models\User;

class UserRepository implements IUserRepository {

    protected $user = null;

    public function getUserByEmail($email)
    {
        $users = User::where('email', '=', $email)->get();
        if (count($users) > 0) {
            $user = $users->first();
        } else {
            $user = null;
        }
        return $user;
    }

    public function createUser($collection = [])
    {
        $user = User::create([
            'name' => $collection['name'],
            'email' => $collection['email'],
            'password' => bcrypt($collection['password'])
        ]);
        return $user;
    }
}
