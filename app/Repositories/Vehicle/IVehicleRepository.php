<?php

namespace App\Repositories\Vehicle;

interface IVehicleRepository {
    public function getVehicles();
    public function createVehicle($collection = []);
    public function getVehicle($id);
    public function updateVehicle($id, $collection = []);
    public function deleteVehicle($id);
}
