<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends ApiController
{

    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'min:3', 'max:255'],
            'password' => ['required', 'string', 'min:6', 'max:255']
        ]);

        $user = $this->userService->getUserByEmail($request->email);
        if ($user == null) {
            return $this->errorResponse('User not found', 422);
        }

        if (!Hash::check($request->password, $user->password)) {
            return $this->errorResponse('User not found', 422);
        }

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        try {
            if (!JWTAuth::attempt($credentials)) {
                return $this->errorResponse('User not found', 422);
            }
        } catch (JWTException $e) {
            return $this->errorResponse('Could not create Token', 500);
        }

        $token = JWTAuth::fromUser($user);

        return $this->showData([
            'token' => $token,
            'user' => $user
        ]);
    }

    public function logout()
    {
        $user = JWTAuth::parseToken()->authenticate();
        JWTAuth::parseToken()->invalidate(true);
        return $this->showData($user);
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'min:3', 'max:255'],
            'email' => ['required', 'string', 'email', 'min:3', 'max:255', 'unique:users,email'],
            'password' => ['required', 'string', 'min:6', 'max:255']
        ]);

        $user = $this->userService->createUser($request->only(['name', 'email', 'password']));

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        try {
            if (!JWTAuth::attempt($credentials)) {
                return $this->errorResponse('User not found', 422);
            }
        } catch (JWTException $e) {
            return $this->errorResponse('Could not create Token', 500);
        }

        $token = JWTAuth::fromUser($user);

        return $this->showData([
            'token' => $token,
            'user' => $user
        ]);
    }
}
