## Sebelum Install Aplikasi

- pastikan anda sudah menginstall [PECL](https://www.php.net/manual/en/install.pecl.php)
- `sudo pecl install mongodb`
- lalu install [MongoDB PHP](https://www.php.net/manual/en/mongodb.installation.pecl.php)

## Cara Install
```
git clone https://gitlab.com/bangcholel/inosoft-test.git
composer install --ignore-platform-reqs
cp .env.example .env
php artisan key:generate
php artisan jwt:secret
```
## Edit File `.env`
```
nano .env
```
Edit pada baris :
```
DB_CONNECTION=mongodb
DB_HOST=127.0.0.1
DB_PORT=27017
DB_DATABASE=inosofttest
DB_USERNAME=
DB_PASSWORD=
```
```
jika sudah simpan filenya lalu jalankan perintah
php artisan serve
```

## [Link Postman](https://documenter.getpostman.com/view/7442635/2s8YeuLBF8)
