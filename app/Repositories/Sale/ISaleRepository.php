<?php

namespace App\Repositories\Sale;

interface ISaleRepository {
    public function getSales();
    public function createSale($collection = []);
    public function getSale($id);
}
