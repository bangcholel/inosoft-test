<?php

namespace Tests\Unit;

use Tests\TestCase;

class AuthTest extends TestCase
{

    public function test_login()
    {
        $data = [
            'email' => 'haris@gmail.com',
        ];
        $this->post(route('api.auth.login'), $data)->assertStatus(422)->assertJson([
            'error' => [
                'msg' => 'Error validation',
                'data' => [
                    'password' => ['The password field is required.']
                ]
            ],
            'code' => 422
        ]);

        $data = [
            'password' => '123456'
        ];
        $this->post(route('api.auth.login'), $data)->assertStatus(422)->assertJson([
            'error' => [
                'msg' => 'Error validation',
                'data' => [
                    'email' => ['The email field is required.']
                ]
            ],
            'code' => 422
        ]);

        $data = [
            'email' => 'haris@gmail.com',
            'password' => '123456'
        ];
        $this->post(route('api.auth.login'), $data)->assertStatus(200)->assertJsonStructure(['data' => ['user', 'token']]);
    }

    public function test_logout()
    {
        $data = [
            'email' => 'haris@gmail.com',
            'password' => '123456'
        ];
        $response = $this->post(route('api.auth.login'), $data)->assertStatus(200)->assertJsonStructure(['data' => ['user', 'token']])->decodeResponseJson();
        $token = 'Bearer ' . $response['data']['token'];
        $this->withHeader('Authorization', $token)->get(route('api.auth.logout'))->assertStatus(200);
    }
}
