<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    protected $fillable = [
        'total_price',
        'motorcycle_total_price',
        'car_total_price',
        'total_item',
        'total_quantity',
        'motorcycle_quantity',
        'car_quantity',
        'motorcycles',
        'cars'
    ];
}
