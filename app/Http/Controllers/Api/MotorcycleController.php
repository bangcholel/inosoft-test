<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Services\MotorcycleService;
use Illuminate\Http\Request;

class MotorcycleController extends ApiController
{
    protected $motorcycleService;

    public function __construct(MotorcycleService $motorcycleService)
    {
        $this->motorcycleService = $motorcycleService;
    }

    public function index() {
        return $this->showAll($this->motorcycleService->getMotorcycles());
    }

    public function store(Request $request) {
        $request->validate([
            'vehicle_id' => ['required', 'string', 'exists:vehicles,_id'],
            'stock' => ['sometimes', 'numeric'],
            'engine' => ['required', 'string', 'max:255'],
            'suspension' => ['required', 'string', 'max:255'],
            'transmission' => ['required', 'string', 'max:255'],
        ]);

        return $this->showData($this->motorcycleService->createMotorcycle($request->all()));
    }

    public function show($id) {
        return $this->showData($this->motorcycleService->getMotorcycle($id));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'vehicle_id' => ['required', 'string', 'exists:vehicles,_id'],
            'stock' => ['sometimes', 'numeric'],
            'engine' => ['required', 'string', 'max:255'],
            'suspension' => ['required', 'string', 'max:255'],
            'transmission' => ['required', 'string', 'max:255'],
        ]);

        return $this->showData($this->motorcycleService->updateMotorcycle($id, $request->all()));
    }

    public function destroy($id) {
        return $this->showData($this->motorcycleService->deleteMotorcycle($id));
    }
}
