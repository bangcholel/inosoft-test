<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Services\CarService;
use Illuminate\Http\Request;

class CarController extends ApiController
{
    protected $carService;

    public function __construct(CarService $carService)
    {
        $this->carService = $carService;
    }

    public function index() {
        return $this->showAll($this->carService->getCars());
    }

    public function store(Request $request) {
        $request->validate([
            'vehicle_id' => ['required', 'string', 'exists:vehicles,_id'],
            'stock' => ['sometimes', 'numeric'],
            'engine' => ['required', 'string', 'max:255'],
            'capacity' => ['required', 'numeric', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
        ]);

        return $this->showData($this->carService->createCar($request->all()));
    }

    public function show($id) {
        return $this->showData($this->carService->getCar($id));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'vehicle_id' => ['required', 'string', 'exists:vehicles,_id'],
            'stock' => ['sometimes', 'numeric'],
            'engine' => ['required', 'string', 'max:255'],
            'capacity' => ['required', 'numeric', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
        ]);

        return $this->showData($this->carService->updateCar($id, $request->all()));
    }

    public function destroy($id) {
        return $this->showData($this->carService->deleteCar($id));
    }
}
