<?php

namespace App\Repositories\Motorcycle;

interface IMotorcycleRepository {
    public function getMotorcycles();
    public function createMotorcycle($collection = []);
    public function getMotorcycle($id);
    public function updateMotorcycle($id, $collection = []);
    public function deleteMotorcycle($id);
}
