<?php
declare(strict_types=1);

namespace App\Services;

use App\Repositories\Motorcycle\MotorcycleRepository;

class MotorcycleService {

    protected $motorcycleRepository;

    public function __construct(MotorcycleRepository $motorcycleRepository) {
        $this->motorcycleRepository = $motorcycleRepository;
    }

    public function getMotorcycles() {
        return $this->motorcycleRepository->getMotorcycles();
    }

    public function createMotorcycle($collection = []) {
        return $this->motorcycleRepository->createMotorcycle($collection);
    }

    public function getMotorcycle($id) {
        return $this->motorcycleRepository->getMotorcycle($id);
    }

    public function updateMotorcycle($id, $collection = []) {
        return $this->motorcycleRepository->updateMotorcycle($id, $collection);
    }

    public function deleteMotorcycle($id) {
        return $this->motorcycleRepository->deleteMotorcycle($id);
    }
}
