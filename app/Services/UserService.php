<?php
declare(strict_types=1);

namespace App\Services;

use App\Repositories\User\UserRepository;

class UserService {

    protected $userRepository;

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    public function getUserByEmail($email) {
        return $this->userRepository->getUserByEmail($email);
    }

    public function createUser($collection = []) {
        return $this->userRepository->createUser($collection);
    }
}
