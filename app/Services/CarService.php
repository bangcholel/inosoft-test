<?php
declare(strict_types=1);

namespace App\Services;

use App\Repositories\Car\CarRepository;

class CarService {

    protected $carRepository;

    public function __construct(CarRepository $carRepository) {
        $this->carRepository = $carRepository;
    }

    public function getCars() {
        return $this->carRepository->getCars();
    }

    public function createCar($collection = []) {
        return $this->carRepository->createCar($collection);
    }

    public function getCar($id) {
        return $this->carRepository->getCar($id);
    }

    public function updateCar($id, $collection = []) {
        return $this->carRepository->updateCar($id, $collection);
    }

    public function deleteCar($id) {
        return $this->carRepository->deleteCar($id);
    }
}
